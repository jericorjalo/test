const User = require('./models/user')
const express = require('express')
const mongoose = require("mongoose");
const bodyParser = require('body-parser')
const njk = require('nunjucks');
const chalk = require('chalk');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const app = express();
const url = 'mongodb://localhost:27017/node-demo';

app.set ('view engine', njk)

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false })) 

// parse application/json
app.use(bodyParser.json())

app.use(cookieParser());
app.use(session({secret: "Shh, its a secret!"}));

mongoose.Promise = global.Promise;

mongoose.connect("mongodb://localhost:27017/node-demo", {},
(err) => {
    if (err) {
        console.log(chalk.red('Hndi maka-connect sa DB'))
    } else {
        console.log(chalk.green('Naka-connect na sa Node-demo'))
    }
});

njk.configure('.', {
    autoescape: true,
    watch: true,
    noCache: true,
    express: app
});

app.get('/', function(req, res) {
    res.render('index.html'); 
});

//Update View
app.get('/update', function(req, res) {
    User.find({}, function(err, users) {
        var userMap = {};
    
        users.forEach(function(user) {
            userMap[user._id] = user;
        });
        res.render('./views/update.html', {userMap});
    });
});

//View
app.get('/view_all', function(req, res) {
    User.find({ active: "true" }, function(err, users) {
        var userMap = {};
    
        users.forEach(function(user) {
            userMap[user._id] = user;
        });
        res.render('./views/view_all.html', {userMap});
    });
});

//Delete View
app.get('/delete', function(req, res) {
    User.find({}, function(err, users) {
        var userMap = {};
    
        users.forEach(function(user) {
            userMap[user._id] = user;
        });
        res.render('./views/delete.html', {userMap});
    });
    
});

app.post('/insert', function(req, res, next) {
    var myData = new User({
        amount: req.body.amount,
        fname: req.body.fname,
        mname: req.body.mname,
        lname: req.body.lname,
        unit: req.body.unit,
        street: req.body.street,
        brangy: req.body.brangy,
        city: req.body.city
    });
      
    myData.save().then(item => {
        res.send("Na sulat na sa Database");
    }).catch(err => {
        res.status(400).send("Di ko pa nasulat sa Database");
    });
});

app.post('/update_process', function(req, res, next){
    User.update({ _id: req.body.userId }, { fname: req.body.fname, mname: req.body.mname, lname: req.body.lname, unit: req.body.unit, street: req.body.street, brangy: req.body.brangy, city: req.body.city}, function (err, raw) {
        if (err) return handleError(err);
        console.log('result', raw);
    });
        res.redirect('./update');
});
app.post('/delete', function(req, res, next){
    if (req.body.active == "true") {
        User.update({ _id: req.body.userId }, { active: false }, function (err, raw) {
            if (err) return handleError(err);
            console.log('result', raw);
        });
    }
    else {
        User.update({ _id: req.body.userId }, { active: true }, function (err, raw) {
            if (err) return handleError(err);
            console.log('result', raw);
        });
    }
    res.redirect('./delete');
});


app.listen(3003, () => console.log('Nakikinig kay 3003!'))
