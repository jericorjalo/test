const mongoose = require("mongoose");

var nameSchema = new mongoose.Schema({
    amount: Number,
    fname: {
        type: String,
        required: 'Kulang ng Pangalan',
        trim: true
    },
    mname: {
        type: String,
        required: 'Kulang ng Gitnang Pangalan',
        trim: true
    },
    lname: {
        type: String,
        required: 'Kulang ng Apelyido',
        trim: true
    },
    unit: {
        type: Number,
        required: 'Kulang ng Unit Numero',
        trim: true
    },
    street: {
        type: String,
        required: 'Kulang ng Street',
        trim: true
    },
    brangy: {
        type: String,
        required: 'Kulang ng Barangay',
        trim: true
    },
    city: {
        type: String,
        required: 'Kulang ng Siyudad',
        trim: true
    },
    active: { 
        type: Boolean, default: true 
    }

});

var User = mongoose.model("User", nameSchema);

module.exports = User